<?php
session_start();
include('Mysqlconn.php');
include('functions_EM_2.php');
$timeout=sessionTimeout();
//Retrieve the parameters from the LogIn page and decide if it is correct or not.

    $email="";
    $password="";
    $Rol="";

  if(isset($_POST["email"]) && isset($_POST["password"]))
    {
        $email = $_POST["email"];
        $password = $_POST["password"];
        logIn($email, $password);
        $timeout="";
    }

    // if(isset($_POST["rol"])) $_SESSION["Rol"]=$_POST['rol'];


    if(isset($_SESSION["Rol"])) $Rol=$_SESSION["Rol"];

    if(!empty($timeout)) {
        redirectToLogInError("TIMEOUT");
    }

    if(empty($Rol)){
        echo "no rol";
        redirectToLogInError("CREDENTIALS");
    }
    function logIn($email, $password){
        $dbName = $GLOBALS["db"];
        $schemaName = $GLOBALS["schema"];
        $pdo = $GLOBALS["pdo"];
        $hashPass=hash("sha256", $password);

        $query="SELECT IdUsuarioBonitaArista, RolARista FROM $dbName.$schemaName.UsuariosBonita WHERE CorreoElectronicoEmpresa= ?";
        $stmt = $pdo->prepare($query);
        $stmt->execute([$email]);

        $RolARista="";
        $idArista ="";

        $user = $stmt->fetch(PDO::FETCH_NUM);
        if(!empty($user)){
            $RolARista=$user[1];
            $idArista =$user[0];

        }else{
            redirectToLogIn();
        }

        echo $RolARista;
        $query="SELECT Password FROM $dbName.$schemaName.RolesArista WHERE RolArista = ?";
        $stmt = $pdo->prepare($query);
        $stmt->execute([$RolARista]);
        
        $hashPassARista;
        $hashPassARista=$stmt->fetch(PDO::FETCH_NUM)[0];

        if(strcmp($hashPass, $hashPassARista)=='0'){
            $_SESSION["Rol"] = $RolARista;
            $_SESSION["ID"] = $idArista;
            logActivity("LI");
        } else{
            redirectToLogIn();
        }
    }
    // function getRol($email, $password) {
    //     $dbName = $GLOBALS["db"];
    //     $schemaName = $GLOBALS["schema"];
    //     $hashPass=hash("sha256", $password);
    //         $pdo = $GLOBALS["pdo"];
    //         $query="SELECT RolARista FROM $dbName.$schemaName.UsuariosBonita WHERE CorreoElectronicoEmpresa= :CorreoElectronicoEmpresa";
    //         $stmt = $pdo->prepare($query);
    //         try {
    //             $stmt->execute(["CorreoElectronicoEmpresa" => $email]);
    //         } catch (\PDOException $e) {
    //             echo "error". $e->getMessage();
    //              throw new \PDOException($e->getMessage(), (int)$e->getCode());
    //         }

    //         $RolARista="";

    //         while ($row = $stmt->fetch(PDO::FETCH_NUM)) 
    //         {
    //         if(empty($row[0])) return "";
    //         $RolARista=$row[0];      
    //         }
            
    //         $query="SELECT Password FROM $dbName.$schemaName.RolesArista WHERE RolArista = :RolArista";
    //         $stmt = $pdo->prepare($query);
    //         try {
    //             $stmt->execute(["RolArista"=> $RolARista]);
    //         } catch (\PDOException $e) {
    //             echo "error". $e->getMessage();
    //             throw new \PDOException($e->getMessage(), (int)$e->getCode());
    //         }
    //         $hashPassARista;
    //         while ($row = $stmt->fetch(PDO::FETCH_NUM)) 
    //         {
    //         $hashPassARista=$row[0];      
    //         }

    //         if(strcmp($hashPass, $hashPassARista)=='0'){
    //             return $RolARista;
    //         } 
    //         else {
    //             redirectToLogIn();
    //         }    
    // }


?>
<html>
    <head>
        <title>BilliB ARista</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- GOOGLE FONTS + MATERIAL ICONS -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- FAVICON -->
        <link rel="shortcut icon" type="image/png" href="https://billibfinance.com/wp-content/uploads/2017/10/favicom.png"/>
        <!-- FRAMEWORKS -->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>         
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-theme.css" rel="stylesheet">
        <link href="css/jquery.dataTables.min.css" rel="stylesheet">
        <!-- CSS -->
        <link rel="stylesheet" href="style/FirstPage.css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

        <!-- FRAMEWORK SCRIPTS -->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script src="//cdn.datatables.net/plug-ins/1.10.16/api/fnFilterOnReturn.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
        <script src="https://cdn.datatables.net/select/1.2.5/js/dataTables.select.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
        <script src="https://cdn.datatables.net/plug-ins/1.10.16/api/sum().js"></script>
        <script type="text/javascript" src="js/firstpage.js"></script>    
    </head>
    <body>
        <header id="BillibHeader">
        <!-- <div  class="row">
        <div class="col s12 m11">
        <h2 id="BHeader" align=\"center\">ARIsta_web. Aplicación de consulta y mantenimiento</h2>
        </div>
        <div style="padding-top: 20px;" class="col s12 m1">
            <a href="functions_route.php?logout=true" class="waves-effect waves-light btn">Log Out </a>
        </div>
        </div> -->
        <h2 id="BHeader" align=\"center\">ARIsta_web. Aplicación de consulta y mantenimiento</h2>
        </header>

        <div id="Preloader">
    <h3 class="preloader-msg">Espere por favor, la aplicación está cargando</h3>
    <div class="preloader-wrapper big active">
      <div class="spinner-layer spinner-blue">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>

      <div class="spinner-layer spinner-red">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>

      <div class="spinner-layer spinner-yellow">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>

      <div class="spinner-layer spinner-green">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>
    </div>
        </div>
        <!-- CONTAINER -->
        <div class="container-fluid main-container">
            <div class="row">
                <!-- LEFT MENU -->
                <div class="col-sm-1 col-md-1 col-lg-1 menu">
                    <div id="leftMenu" class="menu-views">
                                            
                    </div>
                </div>
                <!-- TABLES -->
                <div id="table_container" class="col-sm-11 col-md-11 col-lg-11 tables-container">
                    
                </div>
                <!--<div id="border"></div>-->                  
            </div>
        </div>  
        <footer id="BillibFooter">
            <p style="position: relative; left: 39vw; bottom: 0px; font-weight: 600; font-size: 13px; color: #3a3a3a;">Powered by dyTAB</p>
            <div class="container">
                <center><a href="http://proceedit.blogspot.com.es/" style="color:black; font-size: 13px; font-weight: 600;">Copyright © 2018 Proceedit, all rights reserved.</a>
            </div>
        </footer>       
    </body>
    <!-- OUR SCRIPT! -->
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>-->
    <!--<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <!--<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>-->
    <script type="text/javascript" language="javascript" class="init">

    let userRol = "<?php echo $Rol; ?>";
    let selectbool = false;
    let url_reqs = "functions_route.php";
    let filtersMap = {};
    let inactive = {};
    let exceptionsView = ["FacturasView", "GruposView", "RolesView", "UsuariosView", "MembresiasView"];
    let exceptionsFormat = ["int","long", "smallint", "bigint", "float", "numeric", "double", "decimal"];
    function openTab(tab_name){ 
        var alltabs = document.getElementsByClassName("tab-menu");
        var tab = document.getElementById(tab_name);

   for(i = 0; i <  alltabs.length; i++) {
            alltabs[i].style.backgroundColor = "";
            alltabs[i].style.color = "";
        }

        tab.style.color = "#00ffd1";
        tab.style.backgroundColor = "#15202b";
        tab.style.borderRadius = "3px";
        tab.style.width = "auto";

    }

    function setInnerText(obj, text) {
                var object = document.getElementById(obj);
                object.innerText = text;
            }
    
    function displaytable(table, allviews) 
            {
                var views = allviews.split(",");

                for(var i=0;i<views.length;i++) 
                {
                    var x = document.getElementById("div_" + views[i].trim());
                    x.style.display = "none";
                }
                var y = document.getElementById("div_" + table.trim());
                y.style.display = 'block';
                $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
            }

    function buildLeftBar(){
        $.ajax({
            type: "GET",
            url: url_reqs,
            data: {"left_menu" : true, "Rol": userRol},
            dataType: "json",
            success: function (response) {
                let menu = "<ul>";
                response.tabs.forEach(element => {
                    menu += "<li><a class=\"tab-menu\" id=\"tab-"+element+"\" onclick=\"openTab('tab-"+element+"'),setInnerText('BHeader', '"+element+" - Vista datos')\"href=\"javascript:displaytable('"+element+"View', '"+response.allViews+"');\">"+element+"</a></li>"
                });
                menu +="</ul>";
                $("#leftMenu").html(menu);
            }
        });
    }
    function selectInitiate(){
            $("select").val('10');
            $('select').addClass("browser-default");
            $('select').material_select();
            }

    function buildTableContainer(dataTableFunc){
        $.ajax({
            type: "POST",
            url: url_reqs,
            data: {"create_tables" : true, "Rol": userRol},
            dataType: "json",
            success: function (response) {
                
                let tables = "";
                
                response.tables.forEach(model => {
                    let cells=[];
                    let table = "";
                    let col = 0;
                    table += "<div id=\"div_"+model.name+"\" style=\"display:none; position:absolute; top: 4%; left: 4%; width:100%; table-layout:fixed\">";
                    table += "<table id=\""+model.name+"\" class=\"display dataTable\" cellspacing=\"0\" width=\"100%\">";
                    table += "<thead>";
                    table += "<tr role=\"row\">";
                    let footer ="<tfoot>";
                    let frow1 = "<tr>";
                    let frow2 ="<tr id=\"partials_"+model.name+ "\">";
                    let frow3="<tr id=\"totals_" +model.name+ "\">";
                    let frow4="<tr id=\"minimums_" +model.name+ "\">";
                    let frow5="<tr id=\"maximums_" +model.name+ "\">";
                    let frow6="<tr id=\"selects_"+model.name+"\">";
                    if(!exceptionsView.includes(model.name) && userRol =="admin")
                    {
                        table += "<th class=\"actions\" tabindex=\"0\" rowspan=\"1\" colspan=\"1\" align=\"left\" aria-label=\"Actions\" display=\"inline-block;\" type=\"hidden\">Actions</th>";
                        frow1+="<td class='actionsColumn'></td>";
                        frow2+="<td class='actionsColumn'></td>";
                        frow3+="<td class='actionsColumn'></td>";
                        frow4+="<td class='actionsColumn'></td>";
                        frow5+="<td class='actionsColumn'></td>";
                        frow6+="<td class='actionsColumn'></td>";
                    }                   

                    model.columns.forEach(column => {
                        let sum ="";
                        // let inputSearch = '<input type="text" placeholder="'+title+'" />'
                        if(exceptionsFormat.some(function(element){
                            return column.type.toLowerCase().includes(element);
                        })){
                            sum = "class=\"sum\"";
                            frow2+="<td class=\"partials\" style=\"display:none;\"></td>";
                            frow3+="<td class=\"totals\" style=\"display:none;\"></td>";
                            frow6+="<td class=\"emptySelects\"></td>";
                        }else{
                            frow2+="<td class=\"emptyPartials\"></td>";
                            frow3+="<td class=\"emptyTotals\"></td>";
                            frow6+='<td class="selects_filter" style="display:none; ">'+generateSearchInput("select", model.name, column.name, col)+'</td>';
                        }
                        if(exceptionsFormat.some(function(element){
                            return column.type.toLowerCase().includes(element);
                        }) || column.type.toLowerCase() == 'date'){
                            frow4+='<td class="minimum" style="display:none;">'+generateSearchInput("Min", model.name, column.name, col)+'</td>';
                            frow5+='<td class="maximum" style="display:none;">'+generateSearchInput("Max", model.name, column.name, col)+'</td>';
                        }else{
                            frow4+="<td class=\"emptyMinimum\" data-type='empty'></td>";
                            frow5+="<td class=\"emptyMaximum\" data-type='empty'></td>";
                        }
                        table+= "<th max-height: 50px; tabindex=\"0\" " +sum;
                        table+= " style=\"text-align:left;"+column.width+"\" rowspan=\"1\" colspan=\"1\" aria-label=\"";
                        table+= column.name+": activate to sort column descending\" display=\"inline-block\" aria-sort=\"ascending\" type=\"hidden\">"+column.name+"</th>";
                        frow1+='<td class="columnSearcher Filter'+model.name+'" style="display:none;">'+generateSearchInput("Buscar", model.name, column.name, col)+'</td>';

                        cells[col++]={"filter": "", "range":"", "select":"", "active":false, "max": "", "min": "", "type": column.type};
                    });
                    table+="</tr> </thead>";
                    table+="<tbody></tbody>";
                    frow1+="</tr>";
                    frow2+="</tr>";
                    frow3+="</tr>";
                    frow4+="</tr>";
                    frow5+="</tr>";
                    frow6+="</tr>";
                    filtersMap[model.name]=cells;
                    footer+=frow1+frow2+frow3+frow4+frow5+frow6+"</tfoot>";
                    table+=footer;
                    table+="</table>";
                    table+="</div>";

                    tables += table;

                });
                $("#Preloader").hide()
                $("#table_container").html(tables);

                dataTableFunc();
                
            }
        });
    }
    function columnSearcherCleaner(View) {
            $('.Filter' + View).each( function () {
                if($(this).find('input').val().length) 
                {
                    $(this).find('input').attr("placeholder", "Buscar");
                    $(this).find('input').val('');
                    $(this).find('input').change();
                }
                                                                          
            });
            $('.select_filter_search').each(function(){
                if($(this).val() !== null){
                    $(this).val('');
                    $(this).change();
                }
            });
        }
        function minCleaner() {
            $('.minimum').each( function () {
                if($(this).find('input').val()) {
                    $(this).find('input').attr("placeholder", "Min");
                    $(this).find('input').val('');
                    $(this).find('input').change();
                }
            });
        }

        function maxCleaner() {
            $('.maximum').each( function () {
                if($(this).find('input').val()) {
                    $(this).find('input').attr("placeholder", "Max");
                    $(this).find('input').val('');
                    $(this).find('input').change();
                }
            });
        }
    
    function buildTableContent(){
        let dts = Array.from(document.getElementsByClassName('dataTable'));
            dts.forEach(dtable => {
                inactive[dtable.id] = false;
                let currentTable = $("#"+dtable.id).DataTable({
                // "bProcessing":true,
                // "Processing": true,
                "serverSide":true,
                "ajax":{
                    url: url_reqs,
                    type: "POST",
                    data: function(d){
                        d.fetch_data = true;
                        d.view = dtable.id;
                        d.Rol = userRol;
                        d.inactive = inactive[dtable.id];
                    },
                    error: function(){
                        $("#dataTable_processing").css("display","non");
                    },
                
                },
                "columnDefs":[{ 
                "targets":'actions',
                "orderable":false
                }],
                scrollX:true,
                select: true,
                    autoWidth : true,
                    "language": {
                        "sUrl": "Language/Spanish.json",
                        select:{
                            rows:", %d filas seleccionadas"
                        }
                    },
                    dom: 'Blfrtip',
                    buttons : [
                        {
                            text: 'Filtros',
                            action: function(e, dt, node, config){
                                if($('.columnSearcher').filter(":first").css("display") != "none" ){
                                    $('.columnSearcher').hide();
                                    $(".selects_filter").hide();

                                    $(".btn-filtros").each(function() {
                                        $(this).css("background-color", "#00f1c5");
                                    });

                                    columnSearcherCleaner(dtable.id);
                                }else{
                                    $('.columnSearcher').show();
                                    $(".selects_filter").show();
                                    $(".btn-filtros").each(function() {
                                        $(this).css("background-color", "#1d7d74");
                                    });
                                }
                                $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                            },
                            "className":'btn btn-filtros'
                        },
                        {
                            text: 'Rangos',
                            action: function (e, dt, node, config) {
                                        if($('.minimum').filter(":first").css("display") != "none" ){
                                            $('.minimum').hide();
                                            $('.maximum').hide();
                                            
                                            $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });

                                            minCleaner();
                                            maxCleaner();
                                        }else{
                                            $('.minimum').show();
                                            $('.maximum').show();
                                            $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-rangos'
                                },
                                {
                                    text: 'Parciales',
                                    action: function (e, dt, node, config) {
                                        let rows = document.getElementsByClassName('partials');
                                        let raws = document.getElementsByClassName('emptyPartials');
                                        let hide;
                                        for(let i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide=0;
                                            
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide=1;
                                        }
                                        }
                                       /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-parciales'
                                },
                                {
                                    text: 'Totales',
                                    action: function (e, dt, node, config) {
                                        let rows = document.getElementsByClassName('totals');
                                        let raws = document.getElementsByClassName('emptyTotals');
                                        let hide;
                                        for(let i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                                rows[i].setAttribute('style', '');
                                                raws[i].setAttribute('style', '');
                                                hide=0;
                                                
                                            }
                                            else {
                                                rows[i].style.display='none';
                                                raws[i].style.display='none';
                                                hide=1;
                                            }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-totales'
                                },
                                {
                                    extend: 'selectAll',
                                        action : function(e, dt, button, config){
                                            selectbool = true;
                                            getAll(dtable.id, selectAll);
                                            
                                        },
                                    text:'Todos<i class="material-icons right">done_all</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'selectNone',
                                    text:'Ninguno<i class="material-icons right">remove_circle_outline</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'csv',
                                    bBomInc: true,
                                    text:'CSV<i class="material-icons right">file_download</i>',
                                    exportOptions: {
                                    columns: ':visible:not(:eq(0))',
                                    rows:'.selected',
                                },
                                "className":'btn'
                                },
                                {
                                extend: 'excel',
                                text:'EXCEL<i class="material-icons right">file_download</i>',
                                exportOptions: {
                                rows:'.selected',
                                columns: ':visible:not(:eq(0))',
                                },
                                "className":'btn'
                                },
                                {
                                    text:'Inactivos',
                                    attr:{
                                        id: "inactive"+dtable.id
                                    },
                                    "className":'btn',
                                    action: function(e, dt, node, config){
                                        inactive[dtable.id]=!inactive[dtable.id];
                                        $('#'+dtable.id).DataTable().ajax.reload();
                                        if(inactive[dtable.id]){
                                                    $("#inactive"+dtable.id).css("background-color", "#1d7d74");
                                                    $(".select_filter_search", $("#"+dtable.id).DataTable().table().footer()).data("loaded", false);

                                        }else{
                                                    $("#inactive"+dtable.id).css("background-color", "#00f1c5");
                                                    
                                        }
                                    }
                                },
                                {
                                  text: 'Nuevo Registro<i class=\"material-icons right\" >add_box</i>',
                                  action: function ( e, dt, button, config ) {
                                    window.location = 'Form.php?ViewName='+dtable.id;
                                  },
                                  "className":'btn pulse'        
                                }
                    ],
                    lengthMenu: [
                    [ 25, 50, -1 ],
                    [ '25', '50', 'Todos' ]
                    ],                    
                    initComplete: function () {
                        if(exceptionsView.includes(dtable.id) || userRol !="admin"){
                            $('#'+dtable.id).DataTable().button(".pulse").remove();
                        }
                    },
                    footerCallback: function (row, data, start, end, display) {
                        let api=this.api();
                        api.columns('.sum', {page: 'current'}).every(function (index) {
                        let num = index;
                        let sum = this
                        .data()
                        .reduce( function (a,b) {
                        return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                            let cell = $('tr:eq(1) td:eq(' + num + ')', currentTable.table().footer());
                            cell.html(parseFloat(sum).toFixed(2));
                        });

                        let totalsJson =api.ajax.json().totals;
                        totalsJson.forEach((element, index) => {
                        let num = index;

                        let sum = element;
                        if(sum === null){
                            sum =0;
                        }

                        if(sum >= 0){
                            let cell = $('tr:eq(2) td:not(".actionsColumn"):eq(' + num + ')', currentTable.table().footer());
                            if($(cell).hasClass("totals")){
                            cell.html(parseFloat(sum).toFixed(2));
                            }
                        }
                        });
                    },
                    preDrawCallback: function() {
                        $("#Preloader").show();
                    },
                    drawCallback: function(){
                        $("#Preloader").hide();
                    }

                });

                $('#' + dtable.id + ' tbody').on( 'click', 'tr', function () {
                $(this).toggleClass('selected');
                } );

            });

            selectInitiate();
               
    }
    function getAll(dtableid, callback){
        if($("#"+dtableid).DataTable().page.len()!=-1){
            $("#"+dtableid).DataTable().page.len(-1).draw();
            $("#"+dtableid).on("draw.dt", function(){
                callback(dtableid);
            })
        }else{
            callback(dtableid);
        }

    }
    
    function selectAll(dtableid){
        if(selectbool){
        $("#"+dtableid).DataTable().rows({ search: 'applied'}).select();
        }
        selectbool=false;
    }
    function activateFilterSearch(){
        $(document).on('change', '.filter_search', function(){
            let table = $(this).attr('id').split("_")[0];
            let filterType = $(this).attr('id').split("_")[1];
            let columnNum = $(this).data("col")
            let filterCell = filtersMap[table][columnNum];
            let value;
            if(filterType == "select"){
                value = $(this).children("option:selected").val();
            }else{
                value = $(this).val();
            }
            switch (filterType) {
                case "Buscar":
                    filterCell.filter = value;
                    break;
                
                case "Min":
                    filterCell.min = value;
                    break;
                case "Max":
                    filterCell.max = value;
                    break;
                case "select":
                    filterCell.select = value;
                default:
                    break;
            }

            let searchTerm = getSearchTerm(filterCell);
            $("#"+table).DataTable().columns(columnNum).search(searchTerm);  
            $("#"+table).DataTable().draw();

        });
    }
    
    $(document).ready(function(){
        activateFilterSearch();
        loadSelectOptions()
        buildLeftBar();
        buildTableContainer(buildTableContent);
        $('.datepicker').pickadate(
                { 
                selectMonths: true,
                 selectYears: 15, 
                 today: 'Today',
                 format: 'yyyy-mm-dd', 
                 clear: 'Clear',
                 close: 'Ok',
                 closeOnSelect: false,
                 formatSubmit: 'yyyy-mm-dd'
                });
    
    });
    
    function getSearchTerm(cell){
        let sustMax;
        let sustMin;
        let type;
        switch (cell.type) {
            case "date":
                sustMax = "3000-12-12";
                sustMin = "1800-01-00";
                type = "DATE"
                break;
            default:
                sustMax = 2147483647
                sustMin = -1*2147483647;
                type = "NUM";
                break;
        }
        if(cell.min !="" && cell.max!=""){
            cell.range = "Range;"+cell.min+";"+cell.max+";"+type;
        }
        if(cell.min !="" && cell.max == ""){
            cell.range = "Range;"+cell.min+";"+sustMax+";"+type;
        }
        if(cell.min == "" && cell.max == ""){
            cell.range = "Range;"+sustMin+";"+cell.max+";"+type;
        }
        if(cell.min == "" && cell.max == ""){
            cell.range = "";
        }
        let searchTerm = cell.filter+"|"+cell.range+"|"+cell.select;
        if(searchTerm == "||"){
            searchTerm ="";
        }
        return searchTerm;
    }
    function generateSearchInput(type, table, colname, colnum){
        let input;
        if(type == "select"){
        input='<select style="width: 100%; display:block;" class="select_filter_search filter_search" data-loaded="false" data-col="'+colnum+'" id="'+table+'_'+type+'_'+colname+'"><option value="" selected>Buscar<option></select>';
        }else{
        input = '<input type="text" data-col="'+colnum+'" class="filter_search '+type+'" id="'+table+'_'+type+'_'+colname+'" placeholder="'+type+'" />'
        }
        return input;
    }
    function loadSelectOptions(){
        $(document).on('click', '.select_filter_search', function(){
            let table = $(this).attr('id').split("_")[0];
            let filterType = $(this).attr('id').split("_")[1];
            let columnName = $(this).attr('id').split("_")[2];
            let isLoaded = $(this).data("loaded");
            if(!isLoaded){
            $.ajax({
                type: "POST",
                url: url_reqs,
                data: {
                        "fetch_select_option": true,
                        "table_name" : table,
                        "column_name" : columnName,
                        "Rol" : userRol,
                        "inactive" : inactive[table]
                    },
                dataType: "json",
                success: function (response) {
                    response.forEach(option => {
                        $("#"+table+"_select_"+columnName, $("#"+table).DataTable().table().footer()).append('<option value ="'+option+'">'+option+'</option>');
                    });
                    $("#"+table+"_select_"+columnName, $("#"+table).DataTable().table().footer()).data("loaded", true);
                }
            });
            }
        });
    }

    
    </script>

</html>
