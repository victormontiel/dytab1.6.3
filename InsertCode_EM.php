<?php

include('functions_EM_2.php');
include('Mysqlconn.php');

session_start();

if(isset($_SESSION["Rol"]))
    {
       $Rol=$_SESSION["Rol"];
    }
else 
  {
    redirectToLogIn();
  }

  if(isset($_POST["TableName"]))
    {
        $TableName = $_POST["TableName"];
    }

$columnNames=getColumnNames_table($TableName);

$ViewName=getViewFromTable($TableName);

$values=array();
$rcolumnNames=array();
for($i=0;$i<sizeof($columnNames);$i++) 
{
  $RealFieldName=check_if_PK($columnNames[$i], $TableName);

	if(isset($_POST["$columnNames[$i]"]) && !empty($_POST["$columnNames[$i]"]))
      {
        array_push($values, $_POST["$columnNames[$i]"]);
        array_push($rcolumnNames, $columnNames[$i]);
      }

}

$columnNames=$rcolumnNames;

function createInsertResult($TableName, $values, $columnNames, $Rol){

  ////if(empty($check)) {
  if(empty($Rol)) {
    redirectToLogIn();
    return;
  }
    insertData($TableName, $columnNames, $values);
}

function insertData($TableName, $columnNames, $values) {
  $pdo = $GLOBALS["pdo"];
  $dbName = $GLOBALS["db"];
  $schemaName = $GLOBALS["schema"];
  $idColumn_query="SELECT COLUMN_NAME FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = ? AND TABLE_CATALOG = ? AND (TABLE_NAME = ?) AND (ORDINAL_POSITION = '1')";
  $stmt = $pdo->prepare($idColumn_query);
  $stmt->execute([$schemaName, $dbName, $TableName]);
  while ($row = $stmt->fetch(PDO::FETCH_NUM)) 
    {
        $idColumn=$row[0];   
    }
  
  $idValue_query="SELECT TOP 1 $idColumn FROM $dbName.$schemaName.$TableName ORDER BY $idColumn desc";
  $stmt = $pdo->prepare($idValue_query);
  $stmt->execute();
  while ($row = $stmt->fetch(PDO::FETCH_NUM)) 
    {
        $idValue=$row[0];   
    }
  $idValue++;
  $insert_values=getInsertValues($values, $columnNames, $TableName);
  $insertquery=create_insert_query($TableName, $columnNames, $values, $idColumn);
  try 
  {
    execute_sql($insertquery, $columnNames, $insert_values, $idColumn, $idValue);
    logActivity("INS");
    echo " El registro se ha introducido correctamente.";
  }

  catch(PDOException $e)
  {
    echo "Error: " . $e->getMessage();
    return false;
  }

}


function execute_sql($query, $columnNames, $values, $idCol, $idVal) 
  {
    $pdo = $GLOBALS["pdo"];
    $stmt = $pdo->prepare($query);
    $stmt->bindValue(":$idCol", $idVal);
      for($i=0;$i<sizeof($columnNames);$i++) 
      {
        if(empty($values[$i]) && (string) $values[$i]!=='0') {
          $values[$i]=NULL;
          $stmt->bindValue(":$columnNames[$i]", null, PDO::PARAM_NULL);
        }
        else{
          $stmt->bindValue(":$columnNames[$i]", $values[$i]);
        }
      }
      $stmt->execute();
  }

function check_if_PK($col_name, $TableName)
{
  $pdo= $GLOBALS["pdo"]; //Sebastian
  $query="SELECT ReferencedTableName, ComboValue from $dbName.$schemaName.ForeignKeysAux where TableName= ? and ColumnName= ?"; 
  $stmt = $pdo->prepare($query);
  $stmt->execute([$TableName, $col_name]);
    $finalvalues=array();

  while($row = $stmt->fetch(PDO::FETCH_NUM))
    {
      array_push($finalvalues, $row[0]);
      array_push($finalvalues, $row[1]);
    }

  if(sizeof($finalvalues)==0) return $col_name;
  else return $finalvalues[1];

}

function checkPK_table_inverse($col_name, $TableName)
{
  $pdo= $GLOBALS["pdo"]; //Sebastian
  $query="SELECT ReferencedTableName, ReferencedColumnName from $dbName.$schemaName.ForeignKeysAux where ColumnName=? and TableName= ? ";  
  $stmt = $pdo->prepare($query);
  $stmt->execute([$col_name, $TableName]);
  $finalvalues=array();

  while($row = $stmt->fetch(PDO::FETCH_NUM))
    {
      array_push($finalvalues, $row[0]);
      array_push($finalvalues, $row[1]);
    }

  if(empty($finalvalues)) return "";

  //CIFNIFNIE CLIENTES
  return $finalvalues;
}

function getInsertValues($values, $columnNames, $TableName) 
{
  $pdo= $GLOBALS["pdo"];
  $table_field_value=array(array());
  $final_values=array();

  for($i=0;$i<sizeof($columnNames);$i++) 
  {
    $table_column=checkPK_table_inverse($columnNames[$i], $TableName);
    if(empty($table_column)) 
    {
      $to_push=array($TableName, $columnNames[$i], $values[$i]);
      array_push($table_field_value, $to_push);
      array_push($final_values, $values[$i]);
      continue;
    }

    else 
    {
      if(empty($values[$i]) && (string)$values[$i]!=='0') 
      {
        array_push($final_values, NULL);
      }
      else 
      {
      $id_input=IdFromCombo($table_column[0], $table_column[1], $values[$i], $columnNames[$i], $TableName);
      $to_push=array($table_column[0], $table_column[1], $id_input);
      array_push($table_field_value, $to_push);
      array_push($final_values, $id_input);
      }
      
    }
  }

  return $final_values;

}

function IdFromCombo($table, $column, $value, $columnvalue, $maintable) 
{
  $pdo= $GLOBALS["pdo"]; //Sebastian
  $RealFieldName=check_if_PK($columnvalue, $maintable);
  $query="SELECT $column from $dbName.$schemaName.$table where $RealFieldName=?";
  $stmt = $pdo->prepare($query);
  $stmt->execute([$value]);
  while($row = $stmt->fetch(PDO::FETCH_NUM))
    {
      $ID=$row[0];
    }
  return $ID;
}

function create_insert_query($TableName, $columnNames, $values, $idColumn) 
{
  $dbName = $GLOBALS["db"];
  $schemaName = $GLOBALS["schema"];
  $query="SET IDENTITY_INSERT $dbName.$schemaName.$TableName ON; INSERT INTO $dbName.$schemaName.$TableName ($idColumn, ";
  for($i=0;$i<count($columnNames);$i++) 
  {
    $query = $query . $columnNames[$i] . ", ";
  }
   $query=substr($query, 0, -2) . ") VALUES (:$idColumn, ";
   for($i=0;$i<sizeof($columnNames);$i++) 
   {
     $query .= ":$columnNames[$i], ";

    //  $query .= "$insert_query_value, ";
   }
   $query=substr($query, 0, -2) . ")";
  return $query;
}

function createOptionButtons($ViewName, $Rol) {
  if(empty($Rol)) return;
  echo "<a href='./EFirstPage.php' class=\"btn waves-effect waves-light\"><i class=\"material-icons right\" >add_circle</i>Hacer otro registro</a><br>";
  echo "<form class=\"col s12\" method=\"POST\" action=\"./EFirstPage.php\">";
  echo "<input type =\"hidden\" name=\"rol\" value=\"$Rol\"/>";
  echo "<button class=\"btn waves-effect waves-light\" type=\"submit\"><i class=\"material-icons right\">home</i>Volver a inicio</button>";
  echo "</form>";
}




?>

 <html lang="es">

 	<head>
  	<meta charset="UTF-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="https://billibfinance.com/wp-content/uploads/2017/10/favicom.png"/>
  	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"/>
  	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
  	<link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>
    <title>BilliB ARista</title>
    <!-- CUSTOM CSS -->
    <link type="text/css" rel="stylesheet" href="style/Nuevoregistro_Form.css"/>
    <link type="text/css" rel="stylesheet" href="style/EndPage.css"/>
	</head>
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="materialize/js/materialize.min.js"></script>
  <script>
  function goBackTwice() {
      window.history.go(-2);
    }

    function goBack() {
      window.history.back();
    }

  </script>

	<body>
    <header id="BillibHeader"><h2 align=\"center\">Insert de datos en ARista</h2></header>
    <div class="row">
      <div class="col s12 offset-s4">
        <div class="card">
          <div class="card-update">
            <div class="card-content white-text">
              <?php
              createInsertResult($TableName, $values, $columnNames, $Rol);
              ?>
              <br>
              <?php
              createOptionButtons($ViewName, $Rol);
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <br>
    
    <footer id="BillibFooter">
      <p style="position: relative; left: 39vw;  bottom: 13px; font-weight: 600; font-size: 13px; color: #3a3a3a;">Powered by dyTAB</p>
      <div class="container">
        <center><a href="http://proceedit.blogspot.com.es/" style="color:black; font-size: 13px; font-weight: 600;">Copyright © 2018 Proceedit, all rights reserved.</a>
      </div>
    </footer>
	</body>
</html>