<?php

$clientes_nombre=0;

$clientes_conf=0;

function logActivity($action_code){
	$pdo = $GLOBALS["pdo"];
	$dbName = $GLOBALS["db"];
	$schemaName = $GLOBALS["schema"];

	$action_id = getActionId($action_code);

	$query=" INSERT INTO $dbName.$schemaName.user_actions_log (user_id, user_action_id) VALUES (?,?)";
    $stmt = $pdo->prepare($query);
    $stmt->execute([$_SESSION["ID"], $action_id]);
}

function getActionId($action_code){
	$pdo = $GLOBALS["pdo"];
	$dbName = $GLOBALS["db"];
	$schemaName = $GLOBALS["schema"];

	$query = "SELECT user_action_id from $dbName.$schemaName.user_actions_aux where user_action_code = ?";
	$stmt = $pdo->prepare($query);
	$stmt->execute([$action_code]);
	
	return $stmt->fetch(PDO::FETCH_NUM)[0];
}

function redirectToLogIn(){
	echo "<meta http-equiv=\"refresh\" content=\"0; URL='./LogIn.php?wrong=1'\"/>";
}

function redirectToLogInError($reason) {
	if($reason=="CREDENTIALS") echo "<meta http-equiv=\"refresh\" content=\"0; URL='./LogIn.php?wrong=1'\"/>";
	if($reason=="TIMEOUT") echo "<meta http-equiv=\"refresh\" content=\"0; URL='./LogIn.php?wrong=2'\"/>";
	
}

function getFieldType($table, $column) {
	$pdo = $GLOBALS["pdo"];
	$dbName = $GLOBALS["db"];
	$schemaName = $GLOBALS["schema"];
	$query = "SELECT DATA_TYPE FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = ? AND TABLE_CATALOG = ? AND TABLE_NAME = ? AND COLUMN_NAME = ?";
	$stmt = $pdo->prepare($query);
	$stmt->execute([$schemaName, $dbName, $table, $column]);
	
	$type="";
	while($row = $stmt->fetch(PDO::FETCH_NUM)){
        $type=$row[0];
    }
	return $type;
	
}

function retrieveViewList() 
{
	$pdo = $GLOBALS["pdo"];
	$dbName = $GLOBALS["db"];
	$schemaName = $GLOBALS["schema"];
	// $query = "SHOW FULL TABLES IN ARista WHERE TABLE TYPE LIKE 'VIEW'";
	$query = "SELECT TABLE_NAME from [INFORMATION_SCHEMA].[TABLES] where TABLE_TYPE Like 'VIEW'";
	$stmt = $pdo->prepare($query);
	$stmt->execute();
    $Views = array();
	while ($row = $stmt->fetch(PDO::FETCH_NUM)) 
	{
		if (strpos($row[0], 'Silly') !== false || strpos($row[0], 'Logs') !== false) continue;
		array_push($Views, $row[0]);		
    }

    return $Views;
}
function separateWords($string){
	
	return preg_replace('/(?<! )(?<!^)(?<![A-Z])[A-Z]/', ' $0', $string);
}
function createLeftBar($array, $Rol) 
{
	if(empty($Rol)) {
		return;
	}
	$rows=sizeof($array);
	$viewArray=$array;
	for($i=0;$i<sizeof($array);$i++) {
		$array[$i]=str_replace("View","", $array[$i]);
		// $array[$i]= separateWords($array[$i]);
	}
	$string=getStringFromArray($viewArray);

    $response = array("tabs"=>$array, "allViews"=>$string);
    $jsonResponse = json_encode($response);
    
    return $jsonResponse;
}

function getStringFromArray($array) {
	$string="";
	for($i=0;$i<sizeof($array);$i++) {
		$string=$string . "$array[$i]" . ", ";
	}
	$string=substr($string, 0, -2);
	return $string;
}

function checkLongField($field) {
	$LongFields=array("RazonSocial", "DescripcionCampana", "ConceptoFactura", "Descripcion", "NombreCompleto", "Direccion");
	$superLongFields=array("ObservacionesAccion", "RangoCNAECorto");
	if(in_array($field, $LongFields)) return "min-width:200px;";
	if(in_array($field, $superLongFields)) return "min-width:350px;";



	else return "min-width:50px";
}


function getTableFromView($ViewName)
 {
 	if (strpos($ViewName, 'Contac') !== false) {
    return "PersonasContactoClientes";
	}
	if (strpos($ViewName, 'Accion') !== false) {
    return "AccionesSeguimientoOportunidades";
	}
	if (strpos($ViewName, 'ClientesCamp') !== false) {
    return "ClientesCampanasRel";
	}
	if (strpos($ViewName, 'Facturas') !== false) {
    return "Facturas";
	}
	if (strpos($ViewName, 'Grup') !== false) {
    return "GruposBonita";
	}
	if (strpos($ViewName, 'Memb') !== false) {
    return "MembresiasBonitaRel";
	}
	if (strpos($ViewName, 'Role') !== false) {
    return "RolesBonita";
	}
	if (strpos($ViewName, 'Usuar') !== false) {
    return "UsuariosBonita";
	}

	if (strpos($ViewName, 'Clientes') !== false) {
    return "Clientes";
	}
	if (strpos($ViewName, 'Oport') !== false) {
    return "Oportunidades";
	}
	if (strpos($ViewName, 'Campan') !== false) {
    return "Campanas";
	}


 }

function getColumnNames($ViewName) {
    
    $pdo = $GLOBALS["pdo"];
	$dbName = $GLOBALS["db"];
	$schemaName = $GLOBALS["schema"];
	$querycolumns_names="SELECT (COLUMN_NAME) FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = ? AND TABLE_CATALOG = ? AND TABLE_NAME = ? AND COLUMN_NAME NOT lIKE 'Checksum%' AND COLUMN_NAME NOT LIKE 'Id%' AND COLUMN_NAME NOT LIKE 'Avatar%';";
    $stmt = $pdo->prepare($querycolumns_names);
	$stmt->execute([$schemaName, $dbName, $ViewName]);

    $columnNames = array();

    //if($ViewName=="OportunidadesView") array_push($columnNames, "IdOportunidad");

	while ($row = $stmt->fetch(PDO::FETCH_NUM)) 
	{
		if (strpos($row[0], 'Silly') !== false || strpos($row[0], 'Logs') !== false) continue;
		array_push($columnNames, $row[0]);	
	}



	return $columnNames;
}

function filterColumns($columnNames) {
	$rcolumnNames=array();
  for($i=0;$i<count($columnNames);$i++) {
    if(strpos($columnNames[$i], 'Hito')!==false) continue;
    if(strpos($columnNames[$i], 'NumeroOportunidadesAbiertas')!==false) continue;
    if(strpos($columnNames[$i], 'NumeroClientesAsignados')!==false) continue;
    if(strpos($columnNames[$i], 'NumeroRespuestasPositivas')!==false) continue;
    if(strpos($columnNames[$i], 'ResultadoCampana')!==false) continue;
    else array_push($rcolumnNames, $columnNames[$i]);
  }
  return $rcolumnNames;
}

function getColumnNames_table($TableName) 
{
	$pdo = $GLOBALS["pdo"];
	$dbName = $GLOBALS["db"];
	$schemaName = $GLOBALS["schema"];
	$querycolumns_names="SELECT COLUMN_NAME FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = ? AND TABLE_CATALOG = ? AND (TABLE_NAME = ?) AND (ORDINAL_POSITION <> 1) AND COLUMN_NAME NOT lIKE 'Checksum%' AND COLUMN_NAME NOT like 'IdEstadoActividad%' AND COLUMN_NAME NOT LIKE 'ssma\$rowid%'";
	
	$stmt = $pdo->prepare($querycolumns_names);
	$stmt->execute([$schemaName, $dbName, $TableName]);
	
    $columnNames = array();

	while ($row = $stmt->fetch(PDO::FETCH_NUM)) 
	{
		if (strpos($row[0], 'Silly') !== false || strpos($row[0], 'Logs') !== false) continue;
		array_push($columnNames, $row[0]);		
	}

	return $columnNames;
}

function getColumnNamesString($columnNames) 
{
	$columnsString="";

	for($i=0;$i<sizeof($columnNames);$i++) 
	{
		$columnsString = $columnsString . $columnNames[$i] . ", ";
	}

	$columnsString = $columnsString . "IdEstadoActividad";

	//$columnsString = substr($columnsString, 0, -2);

	return $columnsString;
}
function getSumColumnNamesString($columnNamesAndTypes) 
{
	$numericType = array("int", "long", "smallint", "bigint", "float", "numeric", "double", "decimal");
	$columnsString="";
	$i=0;
	$totalsData;
	foreach ($columnNamesAndTypes as $faKey => $nameAndType) {
		if(array_search($nameAndType["type"], $numericType )!== false){
			$columnsString.="SUM(".$nameAndType['name']."), ";
			$totalsData["'".$i++."'"]=0;
		}else{
			$totalsData[$nameAndType['name']]=null;
		}
	}

	$columnsString = substr($columnsString, 0, -2);
	return array("string" => $columnsString, "array"=>$totalsData);
}

function getPK($ViewName) 
{
	$pdo = $GLOBALS["pdo"];
	$table=getTableFromView($ViewName);
	$querydata = "SELECT CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_NAME = ? AND CONSTRAINT_TYPE ='PRIMARY KEY'";
	$stmt = $pdo->prepare($querydata);
  	$stmt->execute([$table]);
	$data=array();

	while ($row = $stmt->fetch(PDO::FETCH_NUM)) 
	{
		$data = $row;		
	}

	$PK=explode("_", $data[0])[2];

	return $PK;
}

function select_multi($query, $pk_value) 
{
	$pdo = $GLOBALS["pdo"];
	$stmt = $pdo->prepare($query);
  	$stmt->execute([$pk_value]);
	$data=array();

	while ($row = $stmt->fetch(PDO::FETCH_NUM)) 
	{
		array_push($data, $row);		
	}

	return $data;

}

function checkPK_table($col_name, $TableName)
{
	$pdo = $GLOBALS["pdo"]; //Sebastian
	$dbName = $GLOBALS["db"];
  $schemaName = $GLOBALS["schema"];
	$querycheck="SELECT ReferencedTableName, ComboValue from $dbName.$schemaName.ForeignKeysAux where TableName=? and ColumnName=?";	
	$stmt = $pdo->prepare($querycheck);
	$stmt->execute([$TableName, $col_name]);
	$finalvalues=array();

	while($row = $stmt->fetch(PDO::FETCH_NUM))
		{
			array_push($finalvalues, $row[0]);
			array_push($finalvalues, $row[1]);
		}

	if(count($finalvalues)==0) return "";


	//CIFNIFNIE CLIENTES
	return $finalvalues;
}

function createSelect($values, $col_name, $table) 
{
	$createSelectHtml ="";
	$pdo = $GLOBALS["pdo"];
	$dbName = $GLOBALS["db"];
  $schemaName = $GLOBALS["schema"];
	$query = "SELECT " . $values[1] . " FROM $dbName.$schemaName." . $values[0] . " order by " . $values[1];

	$stmt = $pdo->prepare($query);
	$stmt->execute();
	$select_array = array();

	while ($row = $stmt->fetch(PDO::FETCH_NUM)) 
	{
		array_push($select_array, $row[0]);		
	}
	if(strpos($col_name, 'Id')!==false) $col_name2 = substr($col_name, 2);
	else $col_name2=$col_name;

	$mand=check_mandatory($col_name, $table);
	if($mand==1) {
		$star='<span style="color:red">*</span>';
		$required="class=\"required\" oninvalid=\"this.setCustomValidity('Este campo es obligatorio')\" oninput=\"setCustomValidity('')\"";
		$requiredDiv="name=\"requiredSelectDiv\"";
	}
	else {
		$star='';
		$required="";
		$requiredDiv="";
	}

	$createSelectHtml.= "<div class=\"input-field col s12\"" . $requiredDiv . ">
			<select id=\"$table" . "_$col_name\" style=\"width: 400px\" name=\"" . htmlspecialchars($col_name) . "\" " . $required . ">
			//$values[1]
				<option value=\"\" disabled selected>" . $col_name2 . "</option>";
	for($i=0;$i<count($select_array);$i++) 
	{
		$createSelectHtml.= "<option>$select_array[$i]</option>";
	}
	$createSelectHtml.= "</select>";
			$createSelectHtml.= "<label>" . $col_name2 . "$star</label>";
	$createSelectHtml.= "</div>";
	$createSelectHtml.=  "<br>";
	$createSelectHtml.=  "<br>";

	$createSelectHtml.= initialize_select("$table" . "_$col_name");
	
	return $createSelectHtml;

}


function FieldTypetoType($fieldType) {


	if(strpos(strtolower($fieldType), "int")!==false || strpos(strtolower($fieldType), "decimal")!==false || strpos(strtolower($fieldType), "double")!==false) return "\"text\" onkeyup=\"digitsOnly(this);\"";

	return "text";
}

function sessionTimeout()
{
	$timeout = 1800; // Number of seconds until it times out.
 
// Check if the timeout field exists.
if(isset($_SESSION['timeout'])) {
    // See if the number of seconds since the last
    // visit is larger than the timeout period.
    $duration = time() - (int)$_SESSION['timeout'];
    if($duration > $timeout) {
        // Destroy the session and restart it.
        session_destroy();
        session_start();
        return "TIMEOUT";
    }
}
 
// Update the timeout field with the current time.
$_SESSION['timeout'] = time();
return "";

}

function createSimpleField($FieldName, $table) 
{
	$createSimpleFieldHtml ="";
	
	$mand=check_mandatory($FieldName, $table);
	$fieldType=getFieldType($table, $FieldName);

	$type=FieldTypeToType($fieldType);
	if($mand==1) $star='<span style="color:red">*</span>';
	else $star='';
	if (strpos($FieldName, 'Fecha') !== false) 
	{
		$createSimpleFieldHtml.= "<div class=\"row\">
        		<div class=\"input-field col s6\">";
        		if($mand==1) {
        			$createSimpleFieldHtml.= "<input id=\"$FieldName\" type=\"text\" required=\"\" aria-required=\"true\" oninvalid=\"this.setCustomValidity('Este campo es obligatorio')\" oninput=\"setCustomValidity('')\" class=\"datepicker\" name=\"$FieldName\"/>";
        		}
        		else {
        			$createSimpleFieldHtml.= "<input id=\"$FieldName\" type=\"text\" class=\"datepicker\" name=\"$FieldName\"/>";
        		}
          				
          			$createSimpleFieldHtml.= "<label for=\"$FieldName\">$FieldName$star</label>     				
        		</div>
      		 </div>";
	}

	else 
	{	
      	$createSimpleFieldHtml.= "<div class=\"row\">
        		<div class=\"input-field col s6\">";
        		if($mand==1) {
        			$createSimpleFieldHtml.= "<input id=\"$FieldName\" type=" . $type . " required=\"\" aria-required=\"true\" oninvalid=\"this.setCustomValidity('Este campo es obligatorio')\" oninput=\"setCustomValidity('')\" class=\"validate\" name=\"$FieldName\"/>";
        		}
        		else {
        			$createSimpleFieldHtml.= "<input id=\"$FieldName\" type=" . $type . " class=\"validate\" name=\"$FieldName\"/>";
        		}
          				
          		$createSimpleFieldHtml.= "<label for=\"$FieldName\">$FieldName$star</label>
        		</div>
      		 </div> 
      		 ";
	}
	return $createSimpleFieldHtml;
}

function initialize_select($id) 
{
	return "<script type=\"text/javascript\"> 
		$(document).ready(function() {
    		$('#$id').material_select();
  		});
  		</script>";
}

function getViewFromTable($table) 
{
	switch ($table) 
	{
		case 'Clientes':
		return "ClientesView";
		break;

		case 'ClientesDatosOperativosDet':
		return "ClientesView";
		break;

		case 'ClientesDatosEconomicosDet':
		return "ClientesView";
		break;

		case 'ClientesConfiguracionesOperativasDet':
		return "ClientesView";
		break;

		case 'Facturas':
		return "FacturasView";
		break;

		case 'FacturasEvolucionDet':
		return "FacturasView";
		break;

		case 'AccionesSeguimientoOportunidades':
		return "AccionesView";
		break;

		case 'Oportunidades':
		return "OportunidadesView";
		break;

		case 'Campanas':
		return "CampanasView";
		break;

		case 'RolesBonita':
		return "RolesView";
		break;


	}


}
function getColumnNamesAndTypes($ViewName){

    $pdo = $GLOBALS["pdo"];
	$dbName = $GLOBALS["db"];
	$schemaName = $GLOBALS["schema"];
	$querycolumns_names="SELECT COLUMN_NAME FROM information_schema.COLUMNS WHERE TABLE_CATALOG = :TABLE_CATALOG AND TABLE_SCHEMA = :TABLE_SCHEMA AND TABLE_NAME = :TABLE_NAME AND COLUMN_NAME NOT LIKE 'Checksum%' AND COLUMN_NAME NOT LIKE 'Id%' AND COLUMN_NAME NOT LIKE 'Avatar%';";
    $stmt = $pdo->prepare($querycolumns_names);
	$stmt->bindValue(":TABLE_SCHEMA", $schemaName);
	$stmt->bindValue(":TABLE_CATALOG", $dbName);
	$stmt->bindValue(":TABLE_NAME", $ViewName);
	$stmt->execute();

	$columnNames = array();

    //if($ViewName=="OportunidadesView") array_push($columnNames, "IdOportunidad");

	while ($row = $stmt->fetch(PDO::FETCH_NUM)) 
	{
		if (strpos($row[0], 'Silly') !== false || strpos($row[0], 'Logs') !== false) continue;
		array_push($columnNames, $row[0]);	
	}


    $query="SELECT DATA_TYPE FROM information_schema.COLUMNS where TABLE_SCHEMA = ? AND TABLE_CATALOG= ? AND TABLE_NAME = ? AND COLUMN_NAME NOT lIKE 'Checksum%' AND COLUMN_NAME NOT LIKE 'Id%' AND COLUMN_NAME NOT LIKE 'Avatar%'";
	$stmt = $pdo->prepare($query);
	$stmt->execute([$schemaName, $dbName, $ViewName]);
    $types=array();

	while ($row = $stmt->fetch(PDO::FETCH_NUM)) 
	{
		array_push($types,$row[0]);	
    }
    $columns = array();

    foreach ($columnNames as $i => $name) {
        array_push($columns, ["name" => $name, "type" => $types[$i], "width" => checkLongField($name)]);
    }
    return $columns;
    


}
function createAndHideTables($Rol) {

    if(empty($Rol)) {
        //displayLogInErrorMessage();
        redirectToLogIn();
        return;
    }

    //cambiar a tables
    $ViewList=retrieveViewList();

    $tables = array();

    for($i=0;$i<sizeof($ViewList);$i++) {
        
        array_push($tables, ["name"=>$ViewList[$i], "columns"=>getColumnNamesAndTypes($ViewList[$i])]);
    }
    $tablesResponse = array("tables" => $tables);
    $tablesJson = json_encode($tablesResponse);
    return $tablesJson;
}

function getDataTable($View, $Rol, $inactive){
	$pdo = $GLOBALS["pdo"];
	$dbName = $GLOBALS["db"];
  $schemaName = $GLOBALS["schema"];
	$columnNames = getColumnNames($View);
	$columnNamesAndTypes = getColumnNamesAndTypes($View);
	$columnNamesString = getColumnNamesString($columnNames);
	
    $table_name = getTableFromView($View);
	
	$key=getPK($View);
    $params = $cols = $totalRecords = $data = $totalSum= array();

    $params = $_REQUEST;
    $cols = $columnNames;
		$colsFilteredNumber;
		$colsFilteredSearch;
	foreach ($params["columns"] as $i => $column) {
		if(!empty($column["search"]["value"])){
			$colsFilteredNumber[] = $i;
			$colsFilteredSearch[] = $column["search"]["value"];
		}
	}

	$where_condition = $sqlTot = $sqlRec = $sqlSumTotal = "";
	$bindSearch = array();
	if(!empty($params['search']['value'])){
		$where_condition .= "AND ( ";
		foreach ($columnNames as $i => $column) {
			$bindSearch[":search$column"]= "%".$params['search']['value']."%";
			if($i == 0 ){
				$where_condition .= "$column LIKE :search$column ";
			}else{
				$where_condition.= "OR $column LIKE :search$column ";
			}
			if($i == (count($columnNames)-1)){
				$where_condition.= ")";
			}
		}
	}
	if(isset($colsFilteredNumber)){
		//for every column that has a filter on we get their number
		foreach ($colsFilteredNumber as $ikey => $index) {
			//Grab the value of that column search and separate the range filter and column filter with |
			$searchValueArray = explode("|", $colsFilteredSearch[$ikey]);
			foreach ($searchValueArray as $key2 => $filterSearch) {
				if($filterSearch == "") continue;
				switch ($key2) {
					case 0:
						// case for the text filter
						$bindSearch[":search$cols[$index]$key2"] = "%$filterSearch%";
						$where_condition.= "AND ( ".$cols[$index]." LIKE :search$cols[$index]$key2 ) ";
						break;
					case 1:
						// case for the range filter
						$where_condition.= "AND ( ".$cols[$index];
						//separate the min and max range
						$rangeQueryArray= explode(";", $filterSearch);
						//query for range
						// bind min
						$bindSearch[":min$cols[$index]$key2"] = $rangeQueryArray[1];
						// bind max
						$bindSearch[":max$cols[$index]$key2"] = $rangeQueryArray[2];
						// date check is probably obsolete, as binding values in pdo fixed the problem, but it shall stay if something comes up later in dev
						if($rangeQueryArray[3]=="DATE"){
							$where_condition.=" BETWEEN :min$cols[$index]$key2 AND :max$cols[$index]$key2 ) ";
						}else{
							$where_condition.=" BETWEEN :min$cols[$index]$key2 AND :max$cols[$index]$key2 ) ";
						}
						break;
					case 2:
						// case for the select filter
						$bindSearch[":select$cols[$index]$key2"] = "$filterSearch";
						$where_condition.= "AND ( ".$cols[$index]." = :select$cols[$index]$key2 ) ";
						break;
					default:
						// code...
						break;
				}

			}

		}

	}
	
	if($inactive == 'true'){
		$estadoActividad ="1=1";
	}else{
		$estadoActividad="IdEstadoActividad='1'";
	}
	$sql_query = "SELECT $key,".($columnNamesString)." FROM $dbName.$schemaName.$View where $estadoActividad ";
	// foreach ($columnNames as $faKey => $colName) {
		// 	$sql_query_sum_total.=", $colName";
		// }
		
		$sqlTot.=$sql_query;
		$sqlRec.=$sql_query;
	
	$columnSumNameString = getSumColumnNamesString($columnNamesAndTypes);
	$totalSum = $columnSumNameString["array"];
	
	if(!empty($columnSumNameString['string'])){

		$sql_query_sum_total = "SELECT ".$columnSumNameString['string']." FROM $dbName.$schemaName.$View where $estadoActividad ";
		$sqlSumTotal.=$sql_query_sum_total;
		$stmt = $pdo->prepare($sqlSumTotal);
		$stmt->execute();
		
		while($row = $stmt->fetch(PDO::FETCH_NUM)){
			foreach ($row as $keyRow => $sumVal) {
				$totalSum["'".$keyRow."'"] = $sumVal;
			}
		}

	}
	$totalSum = array_values($totalSum);

	if(isset($where_condition) && $where_condition != ''){
		$sqlTot.= $where_condition;
		$sqlRec.= $where_condition;
		// uncomment if you want the totals to be filtered too
		// $sqlSumTotal.= $where_condition;
	}

	$orderBy = $params['order'][0]['column'];
	//when in views that have actions, an extra column is added at the beggining, shifting the whole ennumeration by 1
	if(!in_array($View, array("FacturasView", "GruposView", "RolesView", "UsuariosView", "MembresiasView")) && strcmp($Rol,"admin")===0 && $orderBy!=00)
	{
		$orderBy--;
	}

	if($params['length'] != -1){
		$sqlRec.= " ORDER BY ".$cols[$orderBy]." ".$params['order'][0]['dir']." OFFSET "
		.$params['start']." ROWS FETCH NEXT ".$params['length']." ROWS ONLY";
	}else{
		$sqlRec.= " ORDER BY ".$cols[$orderBy]." ".$params['order'][0]['dir'];
	}

	// $queryTot = sqlsrv_query($conexion, $sqlTot,array(), array( "Scrollable" => 'keyset' ));
	$stmt = $pdo->prepare($sqlTot, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));
	if (isset($bindSearch)){
		foreach ($bindSearch as $keySearch => $bindingValue) {
			$stmt->bindValue($keySearch, $bindingValue);
		}
	}
	$stmt->execute();

	$totalRecords = $stmt->rowCount();

	
	$stmt = $pdo->prepare($sqlRec);
	if (isset($bindSearch)){
		foreach ($bindSearch as $keySearch => $bindingValue) {
			$stmt->bindValue($keySearch, $bindingValue);
		}
	}
	$stmt->execute();
	while( $row = $stmt->fetch(PDO::FETCH_NUM)){
		// REMOVED WHEN DATETIME FIXED
		foreach($row as $faKey => $faValue){
			if($faValue instanceof DateTime){
				$row[$faKey] = $faValue->format("Y-m-d");
			}
		}

		if(!in_array($View, array("FacturasView", "GruposView", "RolesView", "UsuariosView", "MembresiasView")) && strcmp($Rol,"admin")===0)
		{
			if($row[count($row)-1] == '0') {
				$row[0]= "<td style=\"width:40px;\"> <a href=\"./Form.php?Id=$key" . "_" . $row[0] . "&ViewName=$View\"><i class=\"material-icons\">edit</i></a>	
				<a href=\"./DeleteCode.php?Id=$table_name" . "_" . "$key" . "_" . $row[0] . "&ActOrInact=Act\" onclick=\"return confirm('Está seguro de que quiere borrar este registro?')\"> <i class=\"material-icons\">replay</i></a></td>";
			}
			else {
				$row[0]= "<td style=\"text-align:center;width:40px;\"> <a href=\"./Form.php?Id=$key" . "_" . $row[0] . "&ViewName=$View\"> <i class=\"material-icons\">edit</i> </a>	
				<a href=\"./DeleteCode.php?Id=$table_name" . "_" . "$key" . "_" . $row[0] . "&ActOrInact=Inact\" onclick=\"return confirm('Está seguro de que quiere borrar este registro?')\"> <i class=\"material-icons\">delete</i></a></td>";
			}
		}else{
			array_splice($row,0, 1);
		}
		$data[] = $row;
		}
	// $selectOptionsArray = getSelectOptionsArray($View, $columnNamesAndTypes);
	$json_data = array(
		"draw"          =>intval($params['draw']),
		"recordsTotal"  =>intval($totalRecords),
		"recordsFiltered" => intval($totalRecords),
		"data"          =>$data,
		"totals"		=>$totalSum,
		"query"			=>$sqlRec,
		"table"			=>$View,
		"key"			=>$key,
		"sum"			=>$sqlSumTotal
	);
    return json_encode($json_data);

        
	}
	function getSelectOptionsArray($tableName, $colName, $inactive, $rol){
		$pdo = $GLOBALS["pdo"];
		$dbName = $GLOBALS["db"];
  $schemaName = $GLOBALS["schema"];
		$optionsResult=array();
		if($inactive == 'true'){
			$estadoActividad ="";
		}else{
			$estadoActividad="where IdEstadoActividad='1'";
		}
			$query = "SELECT DISTINCT ".$colName." from $dbName.$schemaName.$tableName $estadoActividad";
			$stmt = $pdo->prepare($query);
			$stmt ->execute();
			while ($row = $stmt->fetch(PDO::FETCH_NUM)){
				array_push($optionsResult, $row[0]);
			}
			return json_encode($optionsResult);
		}
	
	// ---------------- FORM SCRIPTS -----------------------

	function createTabsAndForms($ViewName, $action, $id, $Rol) 
{
	if(empty($Rol)) {
		redirectToLogIn();
		return;
	}
	
	$tables=getTablesfromView($ViewName);
	// $tables = array();

	// foreach (getTablesfromView($ViewName) as $key => $tableName) {
	// 	array_push($tables, ["name"=> $tableName, ]);
	// }

	$responseHtml="";
	$responseHtml.= "<div class=\"row\">
    			<div class=\"col s12\">
      				<ul class=\"tabs\">";
	for($i=0;$i<sizeof($tables);$i++) 
	{
		$responseHtml.= "<li class=\"tab col s3\"><a href=\"#$tables[$i]\">$tables[$i]</a></li>";
	}
	$responseHtml.= "</ul></div>";

	for($i=0;$i<sizeof($tables);$i++) 
	{
		$responseHtml.= "<div id=\"$tables[$i]\" class=\"col s12\">";

		$responseHtml.= createForm($tables[$i], $action, $id);

		$responseHtml.= "</div>";

		//FormJQuery($tables[$i]);
	}
	$responseHtml.= "</div>";

	$responseHtml.= "<script type=\"text/javascript\">
			$(document).ready(function(){
    				$('ul.tabs').tabs();
  			});
  			</script>";

	return $responseHtml;

}

function check_mandatory($field, $table) {
	$pdo = $GLOBALS["pdo"];
	$query="SELECT IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = ? AND COLUMN_NAME=?";
	$stmt = $pdo->prepare($query);
    $stmt->execute([$table, $field]);

	while ($row = $stmt->fetch(PDO::FETCH_NUM)) 
	{
		$bool=$row[0];	
	}
	if($bool=='YES') return 0;
	if($bool=='NO') return 1;
}

function getTablesfromView($ViewName) 
{
	$tables=array();
	switch ($ViewName) 
 	{
    	case "ClientesView":
    		array_push($tables, "Clientes");
    		array_push($tables, "ClientesConfiguracionesOperativasDet");
    		array_push($tables, "ClientesDatosEconomicosDet");
    		array_push($tables, "ClientesDatosOperativosDet");
        break;

    	case "OportunidadesView":
        	array_push($tables, "Oportunidades");
        	break;

    	case "CampanasView":
        	array_push($tables, "Campanas");
        	break;
        case "AccionesView":
        	array_push($tables, "AccionesSeguimientoOportunidades");
        	break;
		
		case "ContactosView":
			array_push($tables, "PersonasContactoClientes");
			break;

		case "FacturasView":
			array_push($tables, "Facturas");
			array_push($tables, "FacturasEvolucionDet");
			break;

		case "ClientesCampanasView":
			array_push($tables, "ClientesCampanasRel");
			break;

		case "GruposView":
			array_push($tables, "GruposBonita");
			break;

		case "MembresiasView":
			array_push($tables, "MembresiasBonitaRel");
			break;

		case "RolesView":
			array_push($tables, "RolesBonita");
			break;

		case "UsuariosView":
			array_push($tables, "UsuariosBonita");
		
	}

	return $tables;
}

function createForm($table, $action, $id) 
{
	$createFormHtml ="";
	if($action==0) 
	{
		$createFormHtml.= createInsertForm($table);
	}
	if($action==1) 
	{
		$createFormHtml.= createEditForm($table, $id);
	}
	return $createFormHtml;
}

function createInsertForm($table) 
{
	$createInsertFormHtml ="";

	$columnNames=getColumnNames_table($table);

	$rcolumnNames=filterColumns($columnNames);

	$createInsertFormHtml.= "<fieldset>
    			<form class=\"col s12\" method=\"POST\" action=\"./InsertCode_EM.php\" id=\"$table\">
    				
    				<br><br>";
    $createInsertFormHtml.= "<div class=\"row\">
  			<div class=\"column\">";

    $createInsertFormHtml.= "<input type =\"hidden\" name=\"TableName\" value=\"$table\"/>";
    $createInsertFormHtml.= "<input type =\"hidden\" name=\"mysql\" value=\"insert\"/>";
	
	for($i=0;$i<count($rcolumnNames)/2;$i++) 
	{
		$values=checkPK_table($rcolumnNames[$i], $table);
		if(empty($values)) 
		{
			$createInsertFormHtml.= createSimpleField($rcolumnNames[$i], $table);
		}
		else 
		{
			$createInsertFormHtml.= createSelect($values, $rcolumnNames[$i], $table);
		}

	}

	$createInsertFormHtml.= "</div>";
	$createInsertFormHtml.= "<div class=\"column\">";


	for($j=$i;$j<count($rcolumnNames);$j++) 
	{
		$values=checkPK_table($rcolumnNames[$j], $table);
		if(empty($values)) 
		{
			$createInsertFormHtml.= createSimpleField($rcolumnNames[$j], $table);
		}
		else 
		{
			$createInsertFormHtml.= createSelect($values, $rcolumnNames[$j], $table);
		}


	}
	$createInsertFormHtml.= "</div>";
	$createInsertFormHtml.= "</div>";
	$createInsertFormHtml.= createFormButtons($table, "Insert");
	
	$createInsertFormHtml.= "</form>
			</fieldset>";
	return $createInsertFormHtml;
}

function createFormButtons($table, $action) 
{
	$createFormButtonsHtml ="";
	if($action=="Insert") {
		$createFormButtonsHtml.= "<button class=\"btn waves-effect waves-light pulse\" type=\"submit\" name=\"action\" onclick=\"return checkMandatorySelects(this.form)\">Guardar registro<i class=\"material-icons right\">cloud</i></button>";
		
		//previous button anadir registro
		//onclick=\"return checkMandatorySelects(this.form)\"
		//"<input type=\"submit\" value=\"Añadir registro\" />";
		$createFormButtonsHtml.= "<button type=\"button\" class=\"btn waves-effect waves-light\" onclick=\"window.location.href='./EFirstPage.php'\"><i class=\"material-icons right\">home</i>Volver a la tabla</button>";
		
		$createFormButtonsHtml.= "<button type=\"reset\" class=\"btn waves-effect waves-light\"><i class=\"material-icons right\">autorenew</i>Limpiar</button>";
	}

	if($action=="Update") {
		$createFormButtonsHtml.= "<button class=\"btn waves-effect waves-light\" type=\"submit\" name=\"action\"><i class=\"material-icons right\" onclick=\"return checkMandatorySelects(this.form)\">send</i>Actualizar registro</button>";
		$createFormButtonsHtml.= "<a class=\"btn waves-effect waves-light\" onclick=\"window.location.href='./EFirstPage.php'\"><i class=\"material-icons right\">home</i>Volver a la tabla</a>";
		$createFormButtonsHtml.= "<button type=\"reset\" class=\"btn waves-effect waves-light\"><i class=\"material-icons right\">autorenew</i>Valores iniciales</button>";
	}
	return $createFormButtonsHtml;

}

//confirm('Está seguro de que quiere editar este registro?')

function createEditForm($table, $id) 
{
	$createEditFormHtml ="";
	$id_value = explode("_", $id);

	$data=getDataEdit($table, $id_value[0], $id_value[1]);
	if(empty($data)) {
		$createEditFormHtml.= "No se han registrado datos todavía. Para añadir datos, volver a la tabla y clicar en nuevo registro" . "<br>";
		return;
	}
	$columnNames=getColumnNames_table($table);
	$rcolumnNames=array();
	for($i=0;$i<count($columnNames);$i++) {
		if(strpos($columnNames[$i], 'Hito')!==false) continue;
		else array_push($rcolumnNames, $columnNames[$i]);
	}

	$createEditFormHtml.= "<fieldset>
    			<form class=\"col s12\" method=\"POST\" action=\"./UpdateCode_EM.php\" id=\"$table\">
    				<br><br>";
    $createEditFormHtml.= "<div class=\"row\">
  			<div class=\"column\">";

    $createEditFormHtml.= "<input type =\"hidden\" name=\"TableName\" value=\"$table\"/>";
    $createEditFormHtml.= "<input type =\"hidden\" name=\"Id\" value=\"".htmlspecialchars($id_value[0]). "_" . "$id_value[1]\"/>";
	
	for($i=0;$i<count($rcolumnNames)/2;$i++) 
	{
		$values=checkPK_table($rcolumnNames[$i], $table);
		if(empty($values)) 
		{
			$createEditFormHtml.= createSimpleField_edit($rcolumnNames[$i], $data[0][$i], $table);
		}
		else 
		{
			$createEditFormHtml.= createSelect_edit($values, $rcolumnNames[$i], $table, $data[0][$i]);
		}

	}

	$createEditFormHtml.= "</div>";
	$createEditFormHtml.= "<div class=\"column\">";


	for($j=$i;$j<count($rcolumnNames);$j++) 
	{
		$values=checkPK_table($rcolumnNames[$j], $table);
		if(empty($values)) 
		{
			$createEditFormHtml.= createSimpleField_edit($rcolumnNames[$j], $data[0][$j], $table);
		}
		else 
		{
			$createEditFormHtml.= createSelect_edit($values, $rcolumnNames[$j], $table, $data[0][$j]);
		}
	}

	$createEditFormHtml.= "</div>";
	$createEditFormHtml.= "</div>";
	$createEditFormHtml.= createFormButtons($table, "Update");
	
	$createEditFormHtml.= "</form>
			</fieldset>";
	
	return $createEditFormHtml;

}

function createSelect_edit($values, $col_name, $table, $value) 
{
	$createSelect_edit="";
	$pdo = $GLOBALS["pdo"];
	$dbName = $GLOBALS["db"];
  $schemaName = $GLOBALS["schema"];
	$query = "SELECT " . $values[1] . " FROM $dbName.$schemaName." . $values[0] . " order by " . $values[1];
	$stmt = $pdo->prepare($query);
	$stmt->execute();
	
	$select_array = array();
	
	while ($row = $stmt->fetch(PDO::FETCH_NUM)) 
	{
		array_push($select_array, $row[0]);		
	}
	if(strpos($col_name, 'Id')!==false) $col_name2 = substr($col_name, 2);
	else $col_name2=$col_name;


	//Get Id name

	$query2 = "SELECT ReferencedColumnName FROM $dbName.$schemaName.ForeignKeysAux where ComboValue= ? and TableName=?";
	$stmt = $pdo->prepare($query2);
	$stmt->execute([$values[1], $table]);


	while ($row = $stmt->fetch(PDO::FETCH_NUM)) 
	{
		$id_name=$row[0];		
	}


	$query3 = "SELECT  $values[1]  FROM $dbName.$schemaName.$values[0] where $id_name = ?";
	if($value != ''){
		$stmt = $pdo->prepare($query3);
		$stmt->execute([$value]);

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) 
		{
			if(empty($row[0])) $value_select="";
			else {
				$value_select=$row[0];
			}
					
		}
	}else{
		$value_select=$value;
	}


	if(empty($value_select)) $value_select="";
	$mand=check_mandatory($col_name, $table);
	if($mand==1) {
		$star='<span style="color:red">*</span>';
		$required="required=\"\" aria-required=\"true\"";
		$requiredDiv="name=\"requiredSelectDiv\"";
	}
	else {
		$star='';
		$required="";
		$requiredDiv="";
	}

	$createSelect_edit.= "<div class=\"input-field col s12\"" . $requiredDiv . ">
			<select id=\"$table" . "_$col_name\" style=\"width: 400px\" name=\"" . $col_name . "\"" . $required . ">
				<option value=\" \" disabled selected>" . $col_name2 . "</option>";
	for($i=0;$i<count($select_array);$i++) 
	{
		if($select_array[$i]==$value_select) {
			$createSelect_edit.= "<option selected=\"selected\">".htmlspecialchars($select_array[$i])."</option>";
		}
		else {
			$createSelect_edit.= "<option>".htmlspecialchars($select_array[$i])."</option>";
		}
		
	}
	$createSelect_edit.= "</select>";
	$createSelect_edit.= "<label>" . $col_name2 . $star . "</label>";
	$createSelect_edit.= "</div>";
	$createSelect_edit.=  "<br>";

	$createSelect_edit.=initialize_select("$table" . "_$col_name");
	
	return $createSelect_edit;
}



function createSimpleField_edit($FieldName, $value, $table) 
{
	$createSimpleField_editHtml="";
	if(isset($value)){
		$active="active";
	}else{
		$active="";
	}

	$fieldType=getFieldType($table, $FieldName);
	$type=FieldTypeToType($fieldType);


	$mand=check_mandatory($FieldName, $table);
	if($mand==1) $star='<span style="color:red">*</span>';
	else $star='';
	if (strpos($FieldName, 'Fecha') !== false) 
	{
		if($value instanceof DateTime){
			$value = $value->format('Y-m-d');
		}
		$createSimpleField_editHtml.= "<div class=\"row\">
        		<div class=\"input-field col s6\">";
        		if($mand==1) {
        			$createSimpleField_editHtml.= "<input id=\"$FieldName\" type=\"text\" required=\"\" aria-required=\"true\" oninvalid=\"this.setCustomValidity('Este campo es obligatorio')\" oninput=\"setCustomValidity('')\" class=\"datepicker\" name=\"$FieldName\" value=\"".htmlspecialchars($value)."\"/>";
        		}
        		else {
        			$createSimpleField_editHtml.= "<input id=\"$FieldName\" type=\"text\" class=\"datepicker\" name=\"$FieldName\" value=\"".htmlspecialchars($value)."\"/>";
        		}
          			$createSimpleField_editHtml.= "<label class=\"$active\"  for=\"$FieldName\">$FieldName$star</label>     				
        		</div>
      		 </div>";
	}

	else 
	{	
      	$createSimpleField_editHtml.= "<div class=\"row\">
        		<div class=\"input-field col s6\">";
        		if($mand==1) {
        			$createSimpleField_editHtml.= "<input id=\"$FieldName\" type=" . $type . " required=\"\" aria-required=\"true\" oninvalid=\"this.setCustomValidity('Este campo es obligatorio')\" oninput=\"setCustomValidity('')\" class=\"validate\" name=\"$FieldName\" value=\"".htmlspecialchars($value)."\"/>";
        		}
        		else {
        			$createSimpleField_editHtml.= "<input id=\"$FieldName\" type=" . $type . " class=\"validate\" name=\"$FieldName\" value=\"".htmlspecialchars($value)."\"/>";
        		}
          				
          		$createSimpleField_editHtml.= "<label class=\"$active\" for=\"$FieldName\">$FieldName$star</label>
        		</div>
      		 </div> 
      		 ";
	}
	return $createSimpleField_editHtml;
	
	
}

function getDataEdit($table, $pk, $pk_value) 
{
	$columnNames=getColumnNames_table($table);

	 $columnNamesString=getColumnNamesString($columnNames);
	 
	$query="SELECT ".($columnNamesString)." FROM $table where $pk=?";
	$data=select_multi($query, $pk_value);

	return $data;
}

function createFormHeader($ViewName, $action, $Rol) {
	if(empty($Rol)) return;
	$View = str_replace("View","", $ViewName);
	if($action==0) echo "<header id=\"BillibHeader\"><h2 align=\"center\">$View: Nuevo registro</h2></header>";
	if($action==1) echo "<header id=\"BillibHeader\"><h2 align=\"center\">$View: Editar registro</h2></header>";
}

?>